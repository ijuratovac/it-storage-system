package com.example.demo1;

import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.*;
import java.util.Collections;
import java.util.stream.Stream;

public class CPUController implements Serializable {
    private String path;
    private int index;
    @FXML
    private ListView<CPU> cpus;
    @FXML
    private Label username;
    @FXML
    private TextField manufacturer;
    @FXML
    private TextField model;
    @FXML
    private TextField price;
    @FXML
    private TextField quantity;
    @FXML
    private TextField cores;
    @FXML
    private Label infoLabel;
    @FXML
    private Button btnAdd;
    @FXML
    public void initialize() {
        path = "./database/cpus.txt";
        index = -1;
        infoLabel.setText("");
        if (!new File(path).exists()) {
            try {
                File file = new File(path);
                file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        Deserialize();
    }
    @FXML
    public void setUser(String user) {
        username.setText(user);
    }
    @FXML
    public void onClickBackButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-menu.fxml"));
        Parent root = loader.load();
        MainMenuController mainMenuController = loader.getController();
        mainMenuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("IT Storage System");
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    public void onClickAdd() {
        if (manufacturer.getText().isEmpty()) {
            infoLabel.setText("Please input a manufacturer");
            return;
        }
        if (model.getText().isEmpty()) {
            infoLabel.setText("Please input a model");
            return;
        }
        if (cores.getText().isEmpty()) {
            infoLabel.setText("Please input number of cores");
            return;
        }
        if (price.getText().isEmpty()) {
            infoLabel.setText("Please input a price");
            return;
        }
        if (quantity.getText().isEmpty()) {
            infoLabel.setText("Please input quantity");
            return;
        }
        if (manufacturer.getText().contains(";") || model.getText().contains(";") || cores.getText().contains(";") || price.getText().contains(";") || quantity.getText().contains(";")) {
            infoLabel.setText("Character ; is not allowed");
            return;
        }
        try {
            if (index == -1) {
                cpus.getItems().add(new CPU(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(cores.getText())));
            }
            else {
                cpus.getItems().set(index, new CPU(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(cores.getText())));
            }
        }
        catch (Exception e) {
            infoLabel.setText("Invalid values");
            return;
        }
        Serialize();
        cpus.refresh();
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        cores.setText("");
        btnAdd.setText("Add");
        if (index == -1) {
            infoLabel.setText("New CPU added");
        }
        else {
            infoLabel.setText("CPU updated");
        }
        index = -1;
    }
    @FXML
    public void onClickEdit() {
        index = cpus.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No CPU selected");
        }
        else {
            manufacturer.setText(cpus.getItems().get(index).getManufacturer());
            model.setText(cpus.getItems().get(index).getModel());
            price.setText(String.valueOf(cpus.getItems().get(index).getPrice()));
            quantity.setText(String.valueOf(cpus.getItems().get(index).getQuantity()));
            cores.setText(String.valueOf(cpus.getItems().get(index).getCores()));
            infoLabel.setText("Editing " + manufacturer.getText() + " " + model.getText());
            btnAdd.setText("Save");
        }
    }
    @FXML
    public void onClickDelete() {
        int index = cpus.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No CPU selected");
        }
        else {
            cpus.getItems().remove(index);
            Serialize();
            infoLabel.setText("CPU deleted");
        }
    }
    @FXML
    public void onClickCancel() {
        index = -1;
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        cores.setText("");
        btnAdd.setText("Add");
        infoLabel.setText("");
    }
    @FXML
    public void sortByName() {
        Collections.sort(cpus.getItems(), (cpu1, cpu2)->{
            return (cpu1.getManufacturer() + " " + cpu1.getModel()).compareTo(cpu2.getManufacturer() + " " + cpu2.getModel());
        });
    }
    @FXML
    public void sortByCores() {
        Collections.sort(cpus.getItems(), (cpu1, cpu2)->{
            return Integer.compare(cpu1.getCores(), cpu2.getCores());
        });
    }
    @FXML
    public void sortByPrice() {
        Collections.sort(cpus.getItems(), (cpu1, cpu2)->{
            return Double.compare(cpu1.getPrice(), cpu2.getPrice());
        });
    }
    @FXML
    public void sortByQuantity() {
        Collections.sort(cpus.getItems(), (cpu1, cpu2)->{
            return Integer.compare(cpu1.getQuantity(), cpu2.getQuantity());
        });
    }

    @Override
    public void Serialize() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (CPU cpu : cpus.getItems()) {
                writer.write(cpu.getManufacturer() + ";");
                writer.write(cpu.getModel() + ";");
                writer.write(cpu.getQuantity() + ";");
                writer.write(cpu.getPrice() + ";");
                writer.write(cpu.getCores() + "\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Deserialize() {
        Readable readable = ()->{
            try {
                BufferedReader reader = new BufferedReader(new FileReader(path));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] cpuData = line.split(";");
                    cpus.getItems().add(new CPU(cpuData[0], cpuData[1], Integer.parseInt(cpuData[2]), Double.parseDouble(cpuData[3]), Integer.parseInt(cpuData[4])));
                }
                reader.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        };
        readable.read();
    }
}
