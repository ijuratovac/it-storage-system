package com.example.demo1;

@FunctionalInterface
public interface Readable {
    void read();
}
