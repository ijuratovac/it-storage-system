package com.example.demo1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class MainMenuController {
    @FXML
    Label username;

    @FXML
    public void setUser(String user) {
        username.setText(user);
    }

    @FXML
    public void onClickLogout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    public void viewStoredCPUs(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("CPU.fxml"));
        Parent root = loader.load();
        CPUController cpuController = loader.getController();
        cpuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("CPU Storage");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void viewStoredGPUs(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GPU.fxml"));
        Parent root = loader.load();
        GPUController gpuController = loader.getController();
        gpuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("GPU Storage");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void viewStoredMonitors(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("monitor.fxml"));
        Parent root = loader.load();
        MonitorController monitorController = loader.getController();
        monitorController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Monitor Storage");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void manageUsers(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("users.fxml"));
        Parent root = loader.load();
        UserController userController = loader.getController();
        userController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("User management");
        stage.setScene(scene);
        stage.show();
    }
}
