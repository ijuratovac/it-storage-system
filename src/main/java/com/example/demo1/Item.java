package com.example.demo1;

public abstract class Item {
    protected String manufacturer;
    protected String model;
    protected int quantity;
    protected double price;

    public String getManufacturer() {
        return manufacturer;
    }
    public String getModel() {
        return model;
    }
    public int getQuantity() {
        return quantity;
    }
    public double getPrice() {
        return price;
    }
}