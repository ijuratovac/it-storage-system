package com.example.demo1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.*;
import java.util.Collections;

public class MonitorController implements Serializable {
    private String path;
    private int index;
    @FXML
    private ListView<Monitor> monitors;
    @FXML
    private Label username;
    @FXML
    private TextField manufacturer;
    @FXML
    private TextField model;
    @FXML
    private TextField refreshRate;
    @FXML
    private TextField price;
    @FXML
    private TextField quantity;
    @FXML
    private Label infoLabel;
    @FXML
    private Button btnAdd;
    @FXML
    public void initialize() {
        path = "./database/monitors.txt";
        index = -1;
        infoLabel.setText("");
        if (!new File(path).exists()) {
            try {
                File file = new File(path);
                file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        Deserialize();
    }
    @FXML
    public void setUser(String user) {
        username.setText(user);
    }
    @FXML
    public void onClickBackButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-menu.fxml"));
        Parent root = loader.load();
        MainMenuController mainMenuController = loader.getController();
        mainMenuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("IT Storage System");
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    public void onClickAdd() {
        if (manufacturer.getText().isEmpty()) {
            infoLabel.setText("Please input a manufacturer");
            return;
        }
        if (model.getText().isEmpty()) {
            infoLabel.setText("Please input a model");
            return;
        }
        if (refreshRate.getText().isEmpty()) {
            infoLabel.setText("Please input refresh rate");
            return;
        }
        if (price.getText().isEmpty()) {
            infoLabel.setText("Please input a price");
            return;
        }
        if (quantity.getText().isEmpty()) {
            infoLabel.setText("Please input quantity");
            return;
        }
        if (manufacturer.getText().contains(";") || model.getText().contains(";") || refreshRate.getText().contains(";") || price.getText().contains(";") || quantity.getText().contains(";")) {
            infoLabel.setText("Character ; is not allowed");
            return;
        }
        try {
            if (index == -1) {
                monitors.getItems().add(new Monitor(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(refreshRate.getText())));
            }
            else {
                monitors.getItems().set(index, new Monitor(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(refreshRate.getText())));
            }
        }
        catch (Exception e) {
            infoLabel.setText("Invalid values");
            return;
        }
        Serialize();
        monitors.refresh();
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        refreshRate.setText("");
        btnAdd.setText("Add");
        if (index == -1) {
            infoLabel.setText("New monitor added");
        }
        else {
            infoLabel.setText("Monitor updated");
        }
        index = -1;
    }
    @FXML
    public void onClickEdit() {
        index = monitors.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No monitor selected");
        }
        else {
            manufacturer.setText(monitors.getItems().get(index).getManufacturer());
            model.setText(monitors.getItems().get(index).getModel());
            price.setText(String.valueOf(monitors.getItems().get(index).getPrice()));
            quantity.setText(String.valueOf(monitors.getItems().get(index).getQuantity()));
            refreshRate.setText(String.valueOf(monitors.getItems().get(index).getRefreshRate()));
            infoLabel.setText("Editing " + manufacturer.getText() + " " + model.getText());
            btnAdd.setText("Save");
        }
    }
    @FXML
    public void onClickDelete() {
        int index = monitors.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No monitor selected");
        }
        else {
            monitors.getItems().remove(index);
            Serialize();
            infoLabel.setText("Monitor deleted");
        }
    }
    @FXML
    public void onClickCancel() {
        index = -1;
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        refreshRate.setText("");
        btnAdd.setText("Add");
        infoLabel.setText("");
    }
    @FXML
    public void sortByName() {
        Collections.sort(monitors.getItems(), (m1, m2)->{
            return (m1.getManufacturer() + " " + m1.getModel()).compareTo(m2.getManufacturer() + " " + m2.getModel());
        });
    }
    @FXML
    public void sortByRefreshRate() {
        Collections.sort(monitors.getItems(), (m1, m2)->{
            return Integer.compare(m1.getRefreshRate(), m2.getRefreshRate());
        });
    }
    @FXML
    public void sortByPrice() {
        Collections.sort(monitors.getItems(), (m1, m2)->{
            return Double.compare(m1.getPrice(), m2.getPrice());
        });
    }
    @FXML
    public void sortByQuantity() {
        Collections.sort(monitors.getItems(), (m1, m2)->{
            return Integer.compare(m1.getQuantity(), m2.getQuantity());
        });
    }

    @Override
    public void Serialize() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (Monitor monitor : monitors.getItems()) {
                writer.write(monitor.getManufacturer() + ";");
                writer.write(monitor.getModel() + ";");
                writer.write(monitor.getQuantity() + ";");
                writer.write(monitor.getPrice() + ";");
                writer.write(monitor.getRefreshRate() + "\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Deserialize() {
        Readable readable = ()->{
            try {
                BufferedReader reader = new BufferedReader(new FileReader(path));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] monitorData = line.split(";");
                    monitors.getItems().add(new Monitor(monitorData[0], monitorData[1], Integer.parseInt(monitorData[2]), Double.parseDouble(monitorData[3]), Integer.parseInt(monitorData[4])));
                }
                reader.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        };
        readable.read();
    }
}
