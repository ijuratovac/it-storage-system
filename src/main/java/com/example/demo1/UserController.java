package com.example.demo1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.*;

public class UserController implements Serializable {
    private int index;
    @FXML
    private ListView<User> users;
    @FXML
    private Label username;
    @FXML
    private TextField newUsername;
    @FXML
    private PasswordField newPassword;
    @FXML
    private Label infoLabel;
    @FXML
    private Button btnAdd;
    @FXML
    public void initialize() {
        index = -1;
        infoLabel.setText("");
        Deserialize();
    }
    @FXML
    public void setUser(String user) {
        username.setText(user);
    }
    @FXML
    public void onClickBackButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-menu.fxml"));
        Parent root = loader.load();
        MainMenuController mainMenuController = loader.getController();
        mainMenuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("IT Storage System");
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    public void onClickAdd() {
        if (newUsername.getText().isEmpty()) {
            infoLabel.setText("Please input a username");
            return;
        }
        if (newPassword.getText().isEmpty()) {
            infoLabel.setText("Please input a password");
            return;
        }
        for (User user : users.getItems()) {
            if (newUsername.getText().equals(user.getUsername()) && index == -1) {
                infoLabel.setText("Username is already taken");
                return;
            }
        }
        if (index == -1) {
            users.getItems().add(new User(newUsername.getText(), newPassword.getText()));
        }
        else {
            if (users.getItems().get(index).getUsername().equals(username.getText())) {
                username.setText(newUsername.getText());
            }
            users.getItems().set(index, new User(newUsername.getText(), newPassword.getText()));
        }
        Serialize();
        users.refresh();
        newUsername.setText("");
        newPassword.setText("");
        btnAdd.setText("Add");
        if (index == -1) {
            infoLabel.setText("New user added");
        }
        else {
            infoLabel.setText("User updated");
        }
        index = -1;
    }
    @FXML
    public void onClickEdit() {
        index = users.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No user selected");
            return;
        }
        else {
            newUsername.setText(users.getItems().get(index).getUsername());
            newPassword.setText(users.getItems().get(index).getPassword());
            infoLabel.setText("Editing user " + newUsername.getText());
            btnAdd.setText("Save");
        }
    }
    @FXML
    public void onClickDelete() {
        int index = users.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No user selected");
            return;
        }
        if (users.getItems().get(index).getUsername().equals(username.getText())) {
            infoLabel.setText("Account is currently in use");
            return;
        }
        users.getItems().remove(index);
        Serialize();
        infoLabel.setText("User deleted");
    }
    @FXML
    public void onClickCancel() {
        index = -1;
        newUsername.setText("");
        newPassword.setText("");
        btnAdd.setText("Add");
        infoLabel.setText("");
    }

    @Override
    public void Serialize() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("users.txt"));
            for (User user : users.getItems()) {
                writer.write(user.getUsername() + ";" + user.getPassword() + "\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Deserialize() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("users.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] userData = line.split(";");
                users.getItems().add(new User(userData[0], userData[1]));
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
