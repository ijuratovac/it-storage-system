package com.example.demo1;

public class GPU extends Item implements Stockable {
    private int vram;

    public GPU(String manufacturer, String model, int quantity, double price, int vram) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.quantity = quantity;
        this.price = price;
        this.vram = vram;
    }

    public int getVRAM() {
        return vram;
    }
    public String checkStock() {
        if (quantity > 0) {
            return "In stock: " + quantity;
        }
        return "Out of stock";
    }
    @Override
    public String toString() {
        return manufacturer + " " + model + " " + vram + "GB, " + price + "€, " + checkStock();
    }
}
