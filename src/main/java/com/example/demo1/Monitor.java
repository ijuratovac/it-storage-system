package com.example.demo1;

public class Monitor extends Item implements Stockable {
    private int refreshRate;

    public Monitor(String manufacturer, String model, int quantity, double price, int refreshRate) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.quantity = quantity;
        this.price = price;
        this.refreshRate = refreshRate;
    }

    public int getRefreshRate() {
        return refreshRate;
    }
    @Override
    public String checkStock() {
        if (quantity > 0) {
            return "In stock: " + quantity;
        }
        return "Out of stock";
    }
    @Override
    public String toString() {
        return manufacturer + " " + model + ", " + refreshRate + "Hz, " + price + "€, " + checkStock();
    }
}
