package com.example.demo1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.*;
import java.util.Collections;

public class GPUController implements Serializable {
    private String path;
    private int index;
    @FXML
    private ListView<GPU> gpus;
    @FXML
    private Label username;
    @FXML
    private TextField manufacturer;
    @FXML
    private TextField model;
    @FXML
    private TextField vram;
    @FXML
    private TextField price;
    @FXML
    private TextField quantity;
    @FXML
    private Label infoLabel;
    @FXML
    private Button btnAdd;
    @FXML
    public void initialize() {
        path = "./database/gpus.txt";
        index = -1;
        infoLabel.setText("");
        if (!new File(path).exists()) {
            try {
                File file = new File(path);
                file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        Deserialize();
    }
    @FXML
    public void setUser(String user) {
        username.setText(user);
    }
    @FXML
    public void onClickBackButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main-menu.fxml"));
        Parent root = loader.load();
        MainMenuController mainMenuController = loader.getController();
        mainMenuController.setUser(username.getText());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("IT Storage System");
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    public void onClickAdd() {
        if (manufacturer.getText().isEmpty()) {
            infoLabel.setText("Please input a manufacturer");
            return;
        }
        if (model.getText().isEmpty()) {
            infoLabel.setText("Please input a model");
            return;
        }
        if (vram.getText().isEmpty()) {
            infoLabel.setText("Please input VRAM amount");
            return;
        }
        if (price.getText().isEmpty()) {
            infoLabel.setText("Please input a price");
            return;
        }
        if (quantity.getText().isEmpty()) {
            infoLabel.setText("Please input quantity");
            return;
        }
        if (manufacturer.getText().contains(";") || model.getText().contains(";") || vram.getText().contains(";") || price.getText().contains(";") || quantity.getText().contains(";")) {
            infoLabel.setText("Character ; is not allowed");
            return;
        }
        try {
            if (index == -1) {
                gpus.getItems().add(new GPU(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(vram.getText())));
            }
            else {
                gpus.getItems().set(index, new GPU(manufacturer.getText(), model.getText(), Integer.parseInt(quantity.getText()), Double.parseDouble(price.getText()), Integer.parseInt(vram.getText())));
            }
        }
        catch (Exception e) {
            infoLabel.setText("Invalid values");
            return;
        }
        Serialize();
        gpus.refresh();
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        vram.setText("");
        btnAdd.setText("Add");
        if (index == -1) {
            infoLabel.setText("New GPU added");
        }
        else {
            infoLabel.setText("GPU updated");
        }
        index = -1;
    }
    @FXML
    public void onClickEdit() {
        index = gpus.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No GPU selected");
        }
        else {
            manufacturer.setText(gpus.getItems().get(index).getManufacturer());
            model.setText(gpus.getItems().get(index).getModel());
            price.setText(String.valueOf(gpus.getItems().get(index).getPrice()));
            quantity.setText(String.valueOf(gpus.getItems().get(index).getQuantity()));
            vram.setText(String.valueOf(gpus.getItems().get(index).getVRAM()));
            infoLabel.setText("Editing " + manufacturer.getText() + " " + model.getText());
            btnAdd.setText("Save");
        }
    }
    @FXML
    public void onClickDelete() {
        int index = gpus.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            infoLabel.setText("No GPU selected");
        }
        else {
            gpus.getItems().remove(index);
            Serialize();
            infoLabel.setText("GPU deleted");
        }
    }
    @FXML
    public void onClickCancel() {
        index = -1;
        manufacturer.setText("");
        model.setText("");
        price.setText("");
        quantity.setText("");
        vram.setText("");
        btnAdd.setText("Add");
        infoLabel.setText("");
    }
    @FXML
    public void sortByName() {
        Collections.sort(gpus.getItems(), (gpu1, gpu2)->{
            return (gpu1.getManufacturer() + " " + gpu1.getModel()).compareTo(gpu2.getManufacturer() + " " + gpu2.getModel());
        });
    }
    @FXML
    public void sortByVRAM() {
        Collections.sort(gpus.getItems(), (gpu1, gpu2)->{
            return Integer.compare(gpu1.getVRAM(), gpu2.getVRAM());
        });
    }
    @FXML
    public void sortByPrice() {
        Collections.sort(gpus.getItems(), (gpu1, gpu2)->{
            return Double.compare(gpu1.getPrice(), gpu2.getPrice());
        });
    }
    @FXML
    public void sortByQuantity() {
        Collections.sort(gpus.getItems(), (gpu1, gpu2)->{
            return Integer.compare(gpu1.getQuantity(), gpu2.getQuantity());
        });
    }

    @Override
    public void Serialize() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (GPU gpu : gpus.getItems()) {
                writer.write(gpu.getManufacturer() + ";");
                writer.write(gpu.getModel() + ";");
                writer.write(gpu.getQuantity() + ";");
                writer.write(gpu.getPrice() + ";");
                writer.write(gpu.getVRAM() + "\n");
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Deserialize() {
        Readable readable = ()->{
            try {
                BufferedReader reader = new BufferedReader(new FileReader(path));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] gpuData = line.split(";");
                    gpus.getItems().add(new GPU(gpuData[0], gpuData[1], Integer.parseInt(gpuData[2]), Double.parseDouble(gpuData[3]), Integer.parseInt(gpuData[4])));
                }
                reader.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        };
        readable.read();
    }
}
