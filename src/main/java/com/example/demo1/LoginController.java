package com.example.demo1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LoginController {
    private List<User> users = new ArrayList<>();
    @FXML
    private Label loginInfo;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;

    @FXML
    public void initialize() {
        loginInfo.setText("");
        try {
            File database = new File("database");
            if (!database.exists()) {
                database.mkdir();
            }
            if (new File("users.txt").exists() == false) {
                BufferedWriter writer = new BufferedWriter(new FileWriter("users.txt"));
                writer.write("admin;admin" + "\n");
                writer.close();
            }
            BufferedReader reader = new BufferedReader(new FileReader("users.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] userData = line.split(";");
                users.add(new User(userData[0], userData[1]));
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onClickLogin(ActionEvent event) throws IOException {
        if (username.getText().isEmpty()) {
            loginInfo.setText("Please input your username");
        }
        else if (password.getText().isEmpty()) {
            loginInfo.setText("Please input your password");
        }
        else if (checkLogin(username.getText(), password.getText())){
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main-menu.fxml"));
            Parent root = loader.load();
            MainMenuController mainMenuController = loader.getController();
            mainMenuController.setUser(username.getText());
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root, 600, 400);
            stage.setTitle("IT Storage System");
            stage.setScene(scene);
            stage.show();
        }
        else {
            loginInfo.setText("Wrong username or password");
        }
    }

    private boolean checkLogin(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}