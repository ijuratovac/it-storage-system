package com.example.demo1;

public class CPU extends Item implements Stockable {
    private int cores;

    public CPU(String manufacturer, String model, int quantity, double price, int cores) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.quantity = quantity;
        this.price = price;
        this.cores = cores;
    }

    public int getCores() {
        return cores;
    }
    public String checkStock() {
        if (quantity > 0) {
            return "In stock: " + quantity;
        }
        return "Out of stock";
    }
    @Override
    public String toString() {
        return manufacturer + " " + model + " " + cores + "-Core, " + price + "€, " + checkStock();
    }
}
