package com.example.demo1;

public interface Stockable {
    String checkStock();
}
